package salaire;

import java.util.Date;

public class Manutentionnaire extends Employe implements IAjouterPrime{
	
	private Double heuresBossees;

	public Manutentionnaire(String nom, String prenom, int age, Date dateEmbauche, Double heuresBossees) {
		super(nom, prenom, age, dateEmbauche);
		this.heuresBossees = heuresBossees;
	}

	@Override
	public String toString() {
		return super.toString() + "Manutentionnaire [heuresBossees=" + heuresBossees + "]";
	}

	@Override
	public double ajouterPrimeSecu() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	Double calculerSalaire() {
		// TODO Auto-generated method stub
		return (heuresBossees * 65.0 + ajouterPrimeSecu());
	}

	@Override
	void getNom() {
		// TODO Auto-generated method stub
		System.out.println("Le manutentionnaire " + getNomPrenom());
	}
	
	

}
