package salaire;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import salaire.*;

public class Personnel {
	
	public static List<Employe> creationListPerso () {
		List<Employe> lePersonnel = new ArrayList<Employe>();
		lePersonnel.add(new Vendeur("Birout", "John", 50, new Date("1986/07/24"), 30000.0, 400.0));
		lePersonnel.add(new Representant("Deglingo", "Marcel", 40, new Date("1996/07/24"), 20000.0, 800.0));
		lePersonnel.add(new Producteur("Siffredu", "Rocco", 20, new Date("2016/07/24"), 1000));
		lePersonnel.add(new ProducteurSecu("Lahaye", "Brigitte", 60, new Date("1989/07/24"), 1000, 200.0));
		lePersonnel.add(new Manutentionnaire("Trintignant", "Morice", 80, new Date("1966/07/24"), 45.0));
		lePersonnel.add(new ManutentionnaireSecu("Prost", "Alain", 70, new Date("2000/07/24"), 45.0, 200.0));
		return lePersonnel;
	}
	
	public static void affichagePerso (List<Employe> lePersonnel) {
		Double salaire;
		for(int i = 0; i< lePersonnel.size(); i++) {
			salaire = (lePersonnel.get(i)).calculerSalaire();
			(lePersonnel.get(i)).getNom();
			System.out.println("Salaire : " + salaire + "\n");
		}
	}
	
	public static Double calulSalMoy (List<Employe> lePersonnel) {
		Double salaire, sommeSalaire = 0.0;
		for(int i = 0; i< lePersonnel.size(); i++) {
			salaire = (lePersonnel.get(i)).calculerSalaire();
			sommeSalaire += salaire;
		}
		return (sommeSalaire/(lePersonnel.size()));
	}

}
