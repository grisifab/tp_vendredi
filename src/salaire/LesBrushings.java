package salaire;

import java.util.Date;

public class LesBrushings extends Employe{
	
	private Double chiffreAffairePerso;
	private Double salaireFixe;
	
	
	//Constructeur	
	public LesBrushings(String nom, String prenom, int age, Date dateEmbauche, Double chiffreAffairePerso,
			Double salaireFixe) {
		super(nom, prenom, age, dateEmbauche);
		this.chiffreAffairePerso = chiffreAffairePerso;
		this.salaireFixe = salaireFixe;
	}

	//toString
	@Override
	public String toString() {
		return super.toString() + "LesBrushings [chiffreAffairePerso=" + chiffreAffairePerso + ", salaireFixe=" + salaireFixe + "]";
	}


	@Override
	public double ajouterPrimeSecu() {
		// TODO Auto-generated method stub
		return 0.0;
	}


	@Override
	Double calculerSalaire() {
		// TODO Auto-generated method stub
		
		return (salaireFixe + chiffreAffairePerso*0.2);
	}


	@Override
	void getNom() {
		// TODO Auto-generated method stub	
	}
}
