package salaire;

import java.util.Date;

public class ProducteurSecu extends Producteur implements IAjouterPrime {
	
	private Double prime;

	public ProducteurSecu(String nom, String prenom, int age, Date dateEmbauche, int unitesProduites, Double prime) {
		super(nom, prenom, age, dateEmbauche, unitesProduites);
		this.prime = prime;
	}

	@Override
	public String toString() {
		return super.toString() + "ProducteurSecu [prime=" + prime + "]";
	}
	
	@Override
	public double ajouterPrimeSecu() {
		// TODO Auto-generated method stub
		return prime;
	}

	@Override
	void getNom() {
		// TODO Auto-generated method stub
		System.out.println("Le producteur sÚcuritaire " + getNomPrenom());
	}

}
