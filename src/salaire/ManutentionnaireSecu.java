package salaire;

import java.util.Date;

public class ManutentionnaireSecu extends Manutentionnaire implements IAjouterPrime {
	
	private Double prime;

	public ManutentionnaireSecu(String nom, String prenom, int age, Date dateEmbauche, Double heuresBossees,
			Double prime) {
		super(nom, prenom, age, dateEmbauche, heuresBossees);
		this.prime = prime;
	}

	@Override
	public String toString() {
		return super.toString() + "ManutentionnaireSecu [prime=" + prime + "]";
	}
	
	@Override
	public double ajouterPrimeSecu() {
		// TODO Auto-generated method stub
		return prime;
	}

	@Override
	void getNom() {
		// TODO Auto-generated method stub
		System.out.println("Le manutentionnaire sÚcuritaire " + getNomPrenom());
	}

}
