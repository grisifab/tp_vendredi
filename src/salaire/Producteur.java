package salaire;

import java.util.Date;

public class Producteur extends Employe implements IAjouterPrime{
	
	private int unitesProduites;

	public Producteur(String nom, String prenom, int age, Date dateEmbauche, int unitesProduites) {
		super(nom, prenom, age, dateEmbauche);
		this.unitesProduites = unitesProduites;
	}

	@Override
	public String toString() {
		return super.toString() + "Producteur [unitesProduites=" + unitesProduites + "]";
	}

	@Override
	public double ajouterPrimeSecu() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	Double calculerSalaire() {
		// TODO Auto-generated method stub
		return (unitesProduites*5.0 + ajouterPrimeSecu());
	}

	@Override
	void getNom() {
		// TODO Auto-generated method stub
		System.out.println("Le producteur " + getNomPrenom());
	}
	
	

}
