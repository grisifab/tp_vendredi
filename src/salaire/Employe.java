package salaire;

import java.util.Date;

public abstract class Employe implements IAjouterPrime{
	
	private String nom;
	private String prenom;
	private int age;
	private Date dateEmbauche;
	
	//M�thodes abstraites	
	abstract Double calculerSalaire();
	abstract void getNom();

	
	//Constructeur
	public Employe(String nom, String prenom, int age, Date dateEmbauche) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.age = age;
		this.dateEmbauche = dateEmbauche;
	}
	
	
	public String getNomPrenom() {
		return (prenom + " " + nom);
	}
	
	
	//toString
	@Override
	public String toString() {
		return "Employe [nom=" + nom + ", prenom=" + prenom + ", age=" + age + ", dateEmbauche=" + dateEmbauche + "]";
	}
	

}
